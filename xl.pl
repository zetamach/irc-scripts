# xl.pl -- irssi translation & text-to-speech script
#
# fetter (cc) 2015 #opcode
#
#
# INSTALL 
#
#   Save xl.pl to ~/.irssi/scripts/
# 
#   Skip this step if you don't want to have it load automatically.
#
#     cd ~/.irssi/scripts/ and mkdir autorun if you don't already have one.
# 
#     Link the xl.pl into your autoruns so it will load automatically when irssi starts.  
#
#       cd autorun
#       ln -s ../xl.pl 
#
#   Manual Load 
#     To load and unload the script manually inside irssi:
#
#     /script load xl.pl
#
#     /script unload xl.pl
#
#
# PREREQS: mpg123, normit, spd-say, notify-send
#
#   sudo apt-get install mpg123
#   sudo npm install normit -g
#   spd-say and notify-send are both installed by default on most ubuntu desktops.
#
#
# IRSSI SETTINGS - all settings are off by default so to get it to do anything you have to turn them on.
#
#   /set xl_enabled off
#     This setting lets you easily turn xl on and off while preserving your preferences below.
#
#   /set xl_translate on            
#     Uses normit to translate through translate.google.com.
#
#   /set xl_audio_send on                
#     Uses spd-say to say sent with audio (does not use google by itself).
#     This setting is good if you want to have text-to-speech but don't want all conversation
#     going through google.
#
#   /set xl_audio_recv on                
#     Uses spd-say to say received with audio (does not use google by itself).
#     This setting is good if you want to have text-to-speech but don't want all conversation
#     going through google.
#
#   NOTE:
#   If both audio and translate are on uses normit to translate and mpg123 to speak (both via google).
#
#
# USAGE:
#
#   /xl
#     Show xl usage and help
#
#   /xl stat
#     Show current irssi settings that control operation of xl
#
#
# fetter (cc) 2015 #opcode

use Irssi;
use strict;
use vars qw($VERSION %IRSSI);

$VERSION  = '1.0.0';
%IRSSI    = ();

#Helpers
my $oc_on       = "xl_enabled";
my $oc_xl       = "xl_translate";
my $oc_au_send  = "xl_audio_send";
my $oc_au_recv  = "xl_audio_recv";
my ($enabled, $xl, $au_send, $au_recv);

sub iprint { Irssi::active_win()->print(@_); }

Irssi::settings_add_bool("misc", $oc_on, 0); # is xl enabled - off by default
Irssi::settings_add_bool("misc", $oc_xl, 0); # translate toggle - off by default
Irssi::settings_add_bool("misc", $oc_au_send, 0); # audio send toggle - off by default
Irssi::settings_add_bool("misc", $oc_au_recv, 0); # audio recv toggle - off by default

sub getivar {
  my ($arg) = @_;
  return Irssi::settings_get_bool($arg);
}

# grab defaults
$enabled = getivar($oc_on);
$xl      = getivar($oc_xl);
$au_send = getivar($oc_au_send);
$au_recv = getivar($oc_au_recv);

my $help =  "[ HELP ]\n/xl [source_lang dest_lang message to translate]\n".
            "Example: /xl en zh hi i am speaking in chinese\n".
            "en = english\nfr = french\nru = russian\nes = spanish\npl = polish\nja = japanese\nit = italian\nzh = chinese\n\n".
            "/xl stat\n".
            "Check status of settings. All settings as printed by stat can be set in irssi with /set varname [on|off].\n";


sub parse_cmd {
  my ($data, $server, $witem) = @_;

  if($data eq "") {
    iprint($help);
    return;
  }

  $enabled = getivar($oc_on);
  $xl      = getivar($oc_xl);
  $au_send = getivar($oc_au_send);
  $au_recv = getivar($oc_au_recv);

  if($data =~ m/^stat/) {
    iprint("[ OPTIONS ] (Set with /set var [on|off])");
    iprint("$oc_on = $enabled (xl on/off - preserves prefs)");
    iprint("$oc_xl = $xl (xlates pub msgs thru google)");
    iprint("$oc_au_send = $au_send (sent with spd-say, NO GOOGLE)");
    iprint("$oc_au_recv = $au_recv (received with spd-say, NO GOOGLE)");
    iprint("If translation and either audio option set, sends pub msgs thru google, via normit, to speak and xlate. ".
            "If translate is NOT set, google is NOT used.\n");
    return;
  }

  # sanitize input before sending to shell
  $data =~ s/[=>\$'"&;()\\]//g;

  if($xl && $au_send) {
    iprint($data);
    $witem->command("exec -o normit $data -t");
    return;
  }
  elsif($xl) {
    iprint($data);
    $witem->command("exec -o normit $data");
    return;
  }
  elsif($au_send) {
    $witem->command("exec spd-say '$data'");
    return;
  }
}


sub on_msg {
  my ($server, $data, $nick, $address) = @_;

  $data =~ s/[=>\$'"&;()\\]//g;

  $enabled = getivar($oc_on);
  $xl      = getivar($oc_xl);
  $au_send = getivar($oc_au_send);
  $au_recv = getivar($oc_au_recv);

  if(!$enabled) { return; }

  if($xl && $au_recv) {
    Irssi::active_win()->command("exec normit auto en '$data' -t");
    return;
  }
  elsif($xl) {
    Irssi::active_win()->command("exec normit auto en '$data'");
    return;
  }
  elsif($au_recv) {
    Irssi::active_win()->command("exec spd-say '$data'");
    return;
  }
}

Irssi::command_bind('xl', 'parse_cmd');
Irssi::signal_add_last('message public', 'on_msg');
