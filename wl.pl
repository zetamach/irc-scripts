use strict;
use vars qw($VERSION %IRSSI);

use Irssi;
$VERSION = "0.1";
%IRSSI = (
    authors	=> "fetter",
    contact	=> "zupidstombies\@gmail.com",
    name	=> "WhoLurks",
    description	=> "",
    license	=> "GNU GPL",
    url		=> "",
    changed	=> "",
);

sub cmd_u
{
	my ($data, $server, $channel) = @_;
	my @nicks;
	my $nick;
	
	if ($channel->{type} ne "CHANNEL")
	{
		Irssi::print("You are not on a channel");
		return;
	}

	@nicks = $channel->nicks();

	foreach $nick (@nicks)
	{
    $server->command("whois ". $nick->{nick});
	}
}

sub event_server_event {
    my ($server, $text, $nick, $user) = @_;
    my @items = split(/ /,$text);
    my $type = $items[0];
    
    if ( ($type eq 301) or ($type eq "311") or ($type eq "312") or ($type eq "317") or ($type eq "318") or ($type eq "319") ) 
    {
      my $name = $items[2];
      Irssi::signal_stop();
      print_idletime($name, $server, $items[3]) if ($type eq "317");
    }
}
sub print_idletime {
    my ($name, $ircnet, $time) = @_;
    my $hours = int($time / 3600);
    my $minutes = int(($time % 3600)/60);
    my $seconds = int(($time % 3600)%60);
    Irssi::active_win()->{active}->print($name." is idle for ".$hours." hours, ".$minutes." minutes and ".$seconds." seconds.");
}


Irssi::command_bind('wl','cmd_u');
Irssi::signal_add('server event', 'event_server_event');
