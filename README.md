# README #

Who Lurks, or wl.pl, is an irssi script to whois everyone in the current channel and tell you how long they have been idle.

# INSTALL #

Save wl.pl to ~/.irssi/scripts/

# Autorun - Automatic Load #
Skip this step if you don't want to have it load automatically.

cd ~/.irssi/scripts/ and mkdir autorun if you don't already have one.

Link the wl.pl into your autoruns so it will load automatically when irssi starts.  

cd autorun

ln -s ../wl.pl 

# Manual Load #
To load and unload the script manually inside irssi:

/script load wl.pl

/script unload wl.pl

# Running #

Just type /wl at the irssi command prompt.